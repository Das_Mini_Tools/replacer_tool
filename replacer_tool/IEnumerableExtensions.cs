﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Extras
{
    public static class IEnumerableExtensions
    {
        public static int MorrisPrattSearchFirst<T>(this T[] haystack, T[] needle) where T : IEquatable<T>
        {
            return MorrisPrattSearchFirst(haystack, needle, 0);
        }

        public static int MorrisPrattSearchFirst<T>(this T[] haystack, T[] needle, int startIndex) where T : IEquatable<T>
        {
            return MorrisPrattSearchFirst(haystack, needle, startIndex, haystack.Length - startIndex);
        }

        public static int MorrisPrattSearchFirst<T>(this T[] haystack, T[] needle, int startIndex, int count) where T : IEquatable<T>
        {
            if (haystack == null || needle == null)
                throw new ArgumentNullException("haystack and needle can't be null");

            int tLength = haystack.Length;
            int pLength = needle.Length;
            int rLength = startIndex + count;

            if (startIndex >= tLength)
                throw new ArgumentOutOfRangeException("Value is greater than the length of this array.", "startIndex");

            if (startIndex < 0)
                throw new ArgumentOutOfRangeException("Value is negative.", "startIndex");

            if (rLength > tLength)
                throw new ArgumentOutOfRangeException("Value is greater than the length of this array minus startIndex.", "count");

            if (count < 0)
                throw new ArgumentOutOfRangeException("Value is negative.", "count");

            if (needle.Length > haystack.Length)
                return -1;

            int[] failure = new int[100];

            for (int i = 1, j = failure[0] = -1; i < pLength; i++)
            {
                while (j >= 0 && !needle[j + 1].Equals(needle[i]))
                    j = failure[j];

                if (needle[j + 1].Equals(needle[i]))
                    j++;

                failure[i] = j;
            }

            for (int i = startIndex, j = -1; i < rLength; i++)
            {
                while (j >= 0 && !needle[j + 1].Equals(haystack[i]))
                    j = failure[j];

                if (needle[j + 1].Equals(haystack[i]))
                    j++;

                if (j == needle.Length - 1)
                    return i - needle.Length + 1;
            }

            return -1;
        }

        public static int MorrisPrattSearchFirst<T>(this IEnumerable<T> haystack, IEnumerable<T> needle) where T : IEquatable<T>
        {
            return MorrisPrattSearchFirst(haystack, needle, 0);
        }

        public static int MorrisPrattSearchFirst<T>(this IEnumerable<T> haystack, IEnumerable<T> needle, int startIndex) where T : IEquatable<T>
        {
            return MorrisPrattSearchFirst(haystack, needle, startIndex, haystack.Count() - startIndex);
        }
        
        public static int MorrisPrattSearchFirst<T>(this IEnumerable<T> haystack, IEnumerable<T> needle, int startIndex, int count) where T : IEquatable<T>
        {
            if (haystack == null || needle == null)
                throw new ArgumentNullException("haystack and needle can't be null");

            int tLength = haystack.Count();
            int pLength = needle.Count();
            int rLength = startIndex + count;

            if (startIndex >= tLength)
                throw new ArgumentOutOfRangeException("Value is greater than the length of this array.", "startIndex");

            if (startIndex < 0)
                throw new ArgumentOutOfRangeException("Value is negative.", "startIndex");

            if (rLength > tLength)
                throw new ArgumentOutOfRangeException("Value is greater than the length of this array minus startIndex.", "count");

            if (count < 0)
                throw new ArgumentOutOfRangeException("Value is negative.", "count");

            if (pLength > tLength)
                return -1;

            int[] failure = new int[100];

            for (int i = 1, j = failure[0] = -1; i < pLength; i++)
            {
                while (j >= 0 && !needle.ElementAt(j + 1).Equals(needle.ElementAt(i)))
                    j = failure[j];

                if (needle.ElementAt(j + 1).Equals(needle.ElementAt(i)))
                    j++;

                failure[i] = j;
            }

            for (int i = startIndex, j = -1; i < rLength; i++)
            {
                while (j >= 0 && !needle.ElementAt(j + 1).Equals(haystack.ElementAt(i)))
                    j = failure[j];

                if (needle.ElementAt(j + 1).Equals(haystack.ElementAt(i)))
                    j++;

                if (j == pLength - 1)
                    return i - pLength + 1;
            }

            return -1;
        }

        public static IEnumerable<T[]> Split<T>(this T[] array, T delimiter)
            where T : IEquatable<T>
        {
            if (array == null)
                throw new ArgumentNullException("array");

            if (delimiter == null)
                throw new ArgumentNullException("delimiter");

            Queue<T[]> result = new Queue<T[]>();

            int currentDelimIndex = 0;
            int latestDelimIndex = 0;

            while (currentDelimIndex >= 0)
            {
                currentDelimIndex = Array.IndexOf(array, delimiter, latestDelimIndex);

                if (currentDelimIndex >= 0)
                {
                    result.Enqueue(array.Skip(latestDelimIndex).Take(currentDelimIndex - latestDelimIndex).ToArray());
                    latestDelimIndex = currentDelimIndex + 1;
                }
            }

            int remainLength = array.Length - latestDelimIndex;

            if (remainLength > 0)
                result.Enqueue(array.Skip(latestDelimIndex).Take(remainLength).ToArray());

            return result;
        }
    }
}
