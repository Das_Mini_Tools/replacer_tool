﻿using Extras;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace replacer_tool
{
    internal class Program
    {
        private static void DisplayError(string error)
        {
            Console.WriteLine("ERROR: " + error);
            Console.WriteLine();
            DisplayHelp();
        }

        private static void DisplayHelp()
        {
            AssemblyName assembly = Assembly.GetExecutingAssembly().GetName();

            Console.WriteLine(string.Format("{0} v{1} written by Dasanko for catalinnc", assembly.Name, assembly.Version));
            Console.WriteLine(assembly.Name + " <in.file> <out.file> <hexToReplace> [replacementHex]");
            Console.WriteLine("Usage example:");
            Console.WriteLine(assembly.Name + " raw_potato cooked_potato 0x0D0A");
            Console.WriteLine(assembly.Name + " raw_potato cooked_potato 0x0D0A 0x12");
        }

        private static bool IsInputValid(string[] args)
        {
            if (args.Length == 1 && (args[0].ToLower().Contains("help") || args[0].ToLower().Contains("?")))
            {
                DisplayHelp();
                return false;
            }

            if (args.Length < 3 || args.Length > 4)
            {
                DisplayError("Invalid number of parameters.");
                return false;
            }

            if (!File.Exists(args[0]))
            {
                DisplayError("The specified file does not exist.");
                return false;
            }

            if (!Regex.IsMatch(args[2], @"0x[\da-f]+", RegexOptions.IgnoreCase))
            {
                DisplayError("The specified hex to replace value is not a valid hexadecimal value.");
                return false;
            }

            if (args.Length == 4 && !Regex.IsMatch(args[3], @"0x[\da-f]+", RegexOptions.IgnoreCase))
            {
                DisplayError("The specified replacement hex value is not a valid hexadecimal value.");
                return false;
            }

            return true;
        }

        private static void Main(string[] args)
        {
            if (!IsInputValid(args))
                return;

            byte[] toReplace = HexStringToByteArray(args[2].Substring(2));
            byte[] replacement = HexStringToByteArray(args.Length == 4 ? args[3].Substring(2) : "");
                        
            if (toReplace.Length == 1)
                ReplaceData(args[0], args[1], toReplace, replacement);
            else
            {
                ReplaceData(args[0], args[1] + ".tmp", toReplace, replacement);
                ReplaceData(args[1] + ".tmp", args[1], toReplace, replacement);

                File.Delete(args[1] + ".tmp");
            }
        }
        
        private static void ReplaceData(string inputFile, string outputFile, byte[] toRemove, byte[] replacement)
        {
            using (var sfr = File.OpenRead(inputFile))
            using (var tfw = File.OpenWrite(outputFile))
            {
                byte[] buffer = new byte[4096];
                var bytes = new List<byte>(buffer.Length);
                int readBytes;
                while ((readBytes = sfr.Read(buffer, 0, buffer.Length)) > 0)
                {
                    bytes.Clear();
                    int position = IEnumerableExtensions.MorrisPrattSearchFirst(buffer, toRemove);

                    if (position == -1)
                        tfw.Write(buffer, 0, readBytes);
                    else
                    {
                        while (position != -1)
                        {
                            for (int i = bytes.Count; i < position; i++)
                                bytes.Add(buffer[i]);

                            bytes.InsertRange(position, replacement);

                            position = IEnumerableExtensions.MorrisPrattSearchFirst(bytes, toRemove, position + 1);
                        }
                        for (int i = bytes.Count; i < readBytes; i++)
                            bytes.Add(buffer[i]);

                        tfw.Write(bytes.ToArray(), 0, bytes.Count);
                    }
                }
            }
        }
        
        private static byte[] HexStringToByteArray(string hexString)
        {
            if (hexString.Length % 2 != 0)
                hexString = hexString.Insert(0, "0");
            return Enumerable.Range(0, hexString.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hexString.Substring(x, 2), 16))
                             .ToArray();
        }

    }
}
